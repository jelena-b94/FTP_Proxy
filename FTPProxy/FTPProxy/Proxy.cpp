#include <stdio.h>
#include <windows.h>
#include <string>

#include "Proxy.h"

uint8 Proxy::GetAutomate() {
	return PROXY_AUTOMATE_TYPE_ID;
}

/* This function actually connnects the ClAutoe with the mailbox. */
uint8 Proxy::GetMbxId() {
	return PROXY_AUTOMATE_MBX_ID;
}

uint32 Proxy::GetObject() {
	return GetObjectId();
}

MessageInterface *Proxy::GetMessageInterface(uint32 id) {
	return &StandardMsgCoding;
}

void Proxy::SetDefaultHeader(uint8 infoCoding) {
	SetMsgInfoCoding(infoCoding);
	SetMessageFromData();
}

void Proxy::SetDefaultFSMData() {
	SetDefaultHeader(StandardMessageCoding);
}

void Proxy::NoFreeInstances() {
	printf("[%d] ClAuto::NoFreeInstances()\n", GetObjectId());
}

void Proxy::Reset() {
	printf("[%d] ClAuto::Reset()\n", GetObjectId());
}

void Proxy::waitForClientConnection() {
	// Listen for connections
	cout << "INFO: Waiting for connections" << endl;
	listen(serverSocket, 1);
	// Accept connection
	ftpClientSocket = accept(serverSocket, (struct sockaddr *)&ftpClientAddress, &ftpClientAddressSize);
	if (ftpClientSocket < 0) {
			std::cout << "ERROR: Cannot accept FTP client" << std::endl;
			return;
	}
	cout << "INFO: Client is connected" << endl;

	PrepareNewMessage(0x00, PROXY_MSG_CLIENT_CONNECTION_SUCCESS);
	SetMsgToAutomate(PROXY_AUTOMATE_TYPE_ID);
	SetMsgObjectNumberTo(0);
	SendMessage(PROXY_AUTOMATE_MBX_ID);
	SetState(PROXY_CLIENT_CONNECTED);
}

void Proxy::waitForServerConnection() {
	cout << "INFO: Connectiong to server..." << endl;

	// Connect to FTP Server
	ftpServerSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (ftpServerSocket == INVALID_SOCKET) {
		cout << "ERROR: Cannot open socket for FTP server" << endl;
		return;
	}
	if (connect(ftpServerSocket, (struct sockaddr*) &ftpServerAddress, sizeof(ftpServerAddress)) < 0) {
		cout << "ERROR: Cannot connect to FTP server" << endl;
		closesocket(ftpServerSocket);
		return;
	}
	cout << "INFO: Proxy is connected to FTP server" << endl;


	PrepareNewMessage(0x00, PROXY_MSG_SERVER_CONNECTION_SUCCESS);
	SetMsgToAutomate(PROXY_AUTOMATE_TYPE_ID);
	SetMsgObjectNumberTo(0);
	SendMessage(PROXY_AUTOMATE_MBX_ID);
	SetState(PROXY_READY_FOR_SERVER_RESPONSE);
}

void Proxy::processServerResponse() {
	// Server -> Proxy: Response
	cout << "\nDEBUG: Receiving response from server...\n";
	memset(frame, 0, RECEIVE_BUFFER_LENGTH);
	length = recv(ftpServerSocket, frame, RECEIVE_BUFFER_LENGTH, 0);
	cout << frame << endl;

	//  Server -> Proxy: 2nd response (conditionally)
	if (has2ServerResponses == true) {
		cout << "\nDEBUG: Receiving 2nd response from server...\n";
		memset(frame, 0, RECEIVE_BUFFER_LENGTH);
		length = recv(ftpServerSocket, frame, RECEIVE_BUFFER_LENGTH, 0);
		has2ServerResponses = false;

		if (string(frame).compare(0, 3, "226") == 0) {
			waitForConnection = true;
		}
	} 

	// Proxy -> Client: Response
	cout << "\nDEBUG: Sending response to client...\n";
	if (send(ftpClientSocket, frame, length, 0) <= 0) {
		cout << "ERROR: Response is not sent to client" << endl;
	}

	if (waitForConnection == true) {
		waitForConnection = false;
		PrepareNewMessage(0x00, PROXY_MSG_INITIAL_MSG);
		SetMsgToAutomate(PROXY_AUTOMATE_TYPE_ID);
		SetMsgObjectNumberTo(0);
		SendMessage(PROXY_AUTOMATE_MBX_ID);
		SetState(PROXY_IDLE);
	} else {
		PrepareNewMessage(0x00, PROXY_MSG_RESPONSE_RECEIVED);
		SetMsgToAutomate(PROXY_AUTOMATE_TYPE_ID);
		SetMsgObjectNumberTo(0);
		SendMessage(PROXY_AUTOMATE_MBX_ID);
		SetState(PROXY_READY_FOR_CLINET_REQUEST);
	}
}

void Proxy::processClientRequest() { 
	// Client -> Proxy: Request
	cout << "\nDEBUG: Receiving request from client...\n";
	memset(frame, 0, RECEIVE_BUFFER_LENGTH);
	length = recv(ftpClientSocket, frame, RECEIVE_BUFFER_LENGTH, 0);
	cout << frame << endl;

	if (string(frame).compare(0, 4, "LIST") == 0 || 
		string(frame).compare(0, 4, "RETR") == 0 || 
		string(frame).compare(0, 4, "MLSD") == 0 ||
		string(frame).compare(0, 4, "STOR") == 0) {
		has2ServerResponses = true;
	}

	// Proxy -> Server: Request
	cout << "\nDEBUG: Sending request to server...";
	if (send(ftpServerSocket, frame, length, 0) <= 0) {
		cout << "\nERROR: Command is not sent to server" << endl;
	}

	PrepareNewMessage(0x00, PROXY_MSG_SERVER_CONNECTION_SUCCESS);
	SetMsgToAutomate(PROXY_AUTOMATE_TYPE_ID);
	SetMsgObjectNumberTo(0);
	SendMessage(PROXY_AUTOMATE_MBX_ID);
	SetState(PROXY_READY_FOR_SERVER_RESPONSE);
}

void Proxy::Initialize() {
	SetState(PROXY_IDLE);
	InitEventProc(PROXY_IDLE, PROXY_MSG_INITIAL_MSG, (PROC_FUN_PTR)&Proxy::waitForClientConnection);
	InitEventProc(PROXY_CLIENT_CONNECTED, PROXY_MSG_CLIENT_CONNECTION_SUCCESS, (PROC_FUN_PTR)&Proxy::waitForServerConnection);
	InitEventProc(PROXY_READY_FOR_SERVER_RESPONSE, PROXY_MSG_SERVER_CONNECTION_SUCCESS, (PROC_FUN_PTR)&Proxy::processServerResponse);
	InitEventProc(PROXY_READY_FOR_CLINET_REQUEST, PROXY_MSG_RESPONSE_RECEIVED, (PROC_FUN_PTR)&Proxy::processClientRequest);
}

Proxy::Proxy() : FiniteStateMachine(PROXY_AUTOMATE_TYPE_ID, PROXY_AUTOMATE_MBX_ID, 0, 10, 10) {
}

void Proxy::Start(){
	WSADATA wsaData;
	ftpClientAddressSize = sizeof(ftpClientAddress);
	has2ServerResponses = false;
	waitForConnection = false;

	// Init socket address to FTP server
	ftpServerAddress.sin_family = AF_INET;
	ftpServerAddress.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
	ftpServerAddress.sin_port = htons(FTP_SERVER_PORT);

	// Init acceptance socket address
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons(FTP_PROXY_PORT);

	// Initialise some Microsoft's shit (socket does't work without this)
	if (WSAStartup(MAKEWORD(1,1), &wsaData) == SOCKET_ERROR) {
		std::cout << "ERROR: Error initialising WSA" << std::endl; 
		return;
	}

	// Make socket
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);

	// Check if connection is established
	if (serverSocket == -1) {
		std::cout << "ERROR: Cannot make a server socket" << std::endl; 
		return;
	}

	// Bind
	if (bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1) {
		std::cout << "ERROR: Cannot bind server socket" << std::endl;
		return;
	}


	PrepareNewMessage(0x00, PROXY_MSG_INITIAL_MSG);
	SetMsgToAutomate(PROXY_AUTOMATE_TYPE_ID);
	SetMsgObjectNumberTo(0);
	SendMessage(PROXY_AUTOMATE_MBX_ID);
}

Proxy::~Proxy() {
}