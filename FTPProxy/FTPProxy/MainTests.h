#ifndef MainTestH
#define MainTestH

#include <cppunit/extensions/HelperMacros.h>

#include <stdio.h>
#include <windows.h>
#include <conio.h>

#include "Constants.h"
#include <fsmsystem.h>
#include "../kernel/logfile.h"
#include "Proxy.h"


class MainTests : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(MainTests);
	printf("\n[I]--> Proxy Server Test\n");
	CPPUNIT_TEST(ProxyTestInit);
	CPPUNIT_TEST(ProxyTestServerConnection);
    CPPUNIT_TEST_SUITE_END();
  
private:
    FSMSystem *pSys;
    Proxy * proxy;
    LogFile *lf;
    
public:
    void setUp();
    void tearDown();
    
protected:
	void ProxyTestInit();
	void ProxyTestServerConnection();
};

#endif